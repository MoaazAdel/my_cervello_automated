/// <reference types = "cypress"/>

const { faker } = require("@faker-js/faker");

const apiInstances = `${Cypress.env("apiUrl")}/instances`;
let ownedInstance;

let options = {
  url: "",
  headers: {},
  body: {
    email: faker.internet.email(),
  },
  method: "GET",
};

describe("Invite Instance Admin", () => {
  before("Load Users Data", function () {
    cy.loginByAPI();
    cy.getLocalStorage("access_token").then((token) => {
      options["headers"] = { authorization: `Bearer ${token}` };
    });
    ownedInstance = cy
      .getLocalStorage("ownedInstnace")
      .then((ownedInstance) => {
        options.url = `${apiInstances}/${ownedInstance}/admins`;
      });
  });

  it("POST /admins => Invite instance admin", () => {
    cy.request({
      ...options,
      method: "POST",
      body: {
        email: faker.internet.email(),
      },
    }).then((response) => {
      expect(response.status).to.eq(201);
    });
  });

  it("GET /admins => List all Instance Admins", () => {
    let newOptions = { ...options };
    let countBeforeInviting;
    cy.request({
      ...newOptions,
    }).then((response) => {
      countBeforeInviting = response.body.result.count;
      cy.request({
        ...options,
        method: "POST",
      }).then(() => {
        delete newOptions["body"];
        cy.request({
          ...newOptions,
        }).then((response) => {
          expect(response.status).to.eq(200);
          expect(response.body.result.count).to.eq(countBeforeInviting + 1);
        });
      });
    });
    console.log(ownedInstance);
  });

  it("DELETE instance admin /admins", () => {
    let newOptions = { ...options };
    cy.request({
      ...newOptions,
      method: "POST",
      body: {
        email: faker.internet.email(),
      },
    }).then(() => {
      cy.request({
        ...newOptions,
      }).then((response) => {
        let countBeforeRevoking = response.body.result.count;
        cy.request({
          ...newOptions,
          method: "DELETE",
        }).then((response) => {
          expect(response.body.message).to.eq("SUCCESS");
          cy.request({
            ...newOptions,
          }).then((response) => {
            expect(response.status).to.eq(200);
            expect(response.body.result.count).to.eq(countBeforeRevoking - 1);
          });
        });
      });
    });
  });
});

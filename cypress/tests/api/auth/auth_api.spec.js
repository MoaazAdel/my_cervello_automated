/// <reference types='cypress-tags' />

const { faker } = require("@faker-js/faker");

const pingApi = `${Cypress.env("apiUrl")}/ping`;
const loginAPIUrl = `${Cypress.env("apiUrl")}/auth`;
const registerAPIUrl = `${Cypress.env("apiUrl")}/register`;
const accountsApiUrl = `${Cypress.env("apiUrl")}/accounts`;
const instancesApiUrl = `${Cypress.env("apiUrl")}/instances`;
const createAccountApiUrl = `${Cypress.env("apiUrl")}/create-account`;
const userName = Cypress.env("userName");
const password = Cypress.env("password");
const randomEmail = faker.internet.email();
const randomInstanceName = faker.name.firstName();

describe("Auth APIs Tests", { tags: ["@smoke"] }, () => {
  it("Test Ping", { tags: "wez" }, () => {
    cy.request("GET", pingApi).then((resp) => {
      expect(resp.status).to.eq(200);
      expect(resp.body.message).to.eq("pong");
    });
  });

  context("Login Tests", () => {
    it.only("Should successfully login using the API", () => {
      cy.request({
        method: "GET",
        url: loginAPIUrl,
        body: {
          username: userName,
          password: password,
          grantType: "password",
        },
      }).then((resp) => {
        expect(resp.status).to.eq(200);
        expect(resp.body).to.have.any.keys("access_token");
      });
    });
  });

  it("Should get 401 for invalid username or pass", () => {
    cy.request({
      method: "POST",
      url: loginAPIUrl,
      body: {
        username: "Invalid",
        password: "Iotbl55$",
        grantType: "password",
      },
      failOnStatusCode: false,
    }).then((resp) => {
      expect(resp.status).to.eq(401);
    });
  });

  context("SignUp Tests", () => {
    it("Should Create Instance", () => {
      cy.request({
        method: "POST",
        url: loginAPIUrl,
        body: {
          username: "madel@iotblue.net",
          password: "Udq3ydGP75fRSVm$",
          grantType: "password",
        },
      }).then((resp) => {
        expect(resp.status).to.eq(200);
        let token = resp.body.access_token;
        cy.request({
          url: accountsApiUrl,
          method: "POST",
          headers: {
            authorization: `Bearer ${token}`,
          },
          body: {
            email: randomEmail,
          },
        }).then((resp) => {
          expect(resp.status).to.eq(201);
          let resultLink = resp.body.result;
          let token = resultLink.split("=").pop();
          cy.request({
            url: createAccountApiUrl,
            method: "POST",
            body: {
              token: token,
              password: "Iotblue55$",
              firstName: "232",
              lastName: "333",
              consent: true,
            },
          }).then((resp) => {
            expect(resp.status).to.eq(200);
            cy.request({
              url: loginAPIUrl,
              method: "POST",
              body: {
                username: randomEmail,
                password: "Iotblue55$",
                grantType: "password",
              },
            }).then((resp) => {
              expect(resp.status).to.eq(200);
              let token = resp.body.access_token;
              cy.request({
                url: instancesApiUrl,
                method: "POST",
                headers: {
                  authorization: `Bearer ${token}`,
                },
                body: {
                  name: randomInstanceName,
                  firstProjectType: "STEM",
                },
              }).then((resp) => {
                expect(resp.status).to.eq(201);
                cy.request({
                  url: instancesApiUrl,
                  method: "GET",
                  headers: {
                    authorization: `Bearer ${token}`,
                  },
                }).then((resp) => {
                  expect(resp.status).to.eq(200);
                  expect(resp.body.result.instanceOwned).to.haveOwnProperty(
                    "id"
                  );
                });
              });
            });
          });
        });
      });
    });
    it("Should Send Sign-up Link Successfully when providing valid email", () => {
      cy.request({
        method: "POST",
        url: registerAPIUrl,
        body: {
          email: faker.internet.email(),
        },
      })
        .its("status")
        .should("eq", 200);
    });
  });

  it("Registration Email Validation", () => {
    cy.request({
      method: "POST",
      url: registerAPIUrl,
      body: {
        email: "Invalid",
      },
      failOnStatusCode: false,
    })
      .its("status")
      .should("eq", 400);
    //No Email provided
    cy.request({
      method: "POST",
      url: registerAPIUrl,
      body: {},
      failOnStatusCode: false,
    }).then((resp) => {
      expect(resp.body.result).to.eq("Email is a required field");
    });
  });
});

/// <reference types="cypress"/>

import { loginPage } from "../../pages/auth/login.page";
import { chooseInstancePage } from "../../pages/instance/choose-instance-page";
import { signUpPage } from "../../pages/auth/sign-up.page";
import { magicLinkPage } from "../../pages/auth/magic-link.page";
import { Selectors } from "../../pages/auth/auth-selectors";

const { faker } = require("@faker-js/faker");

let newRandomUserEmail = faker.internet.email();
const userName = Cypress.env("userName");
const password = Cypress.env("password");

beforeEach("Visit home page", () => {
  // Cypress.Cookies. preserveOnce("access_token");
  cy.visit("/");
});

describe("Auth Tests", function () {
  context("Login Tests", () => {
    it("Should login successfully and navigates to 'Choose Instance page'", function () {
      loginPage.login(userName, password);
      chooseInstancePage.verifyWelcomeBackMessage();
    });

    it("should remember a user for 30 days after login", function () {
      loginPage.login(userName, password);
      cy.intercept("GET", "**/instances").as("instances");
      cy.wait("@instances");
      cy.getCookie("CERVELLO_REMEMBER_ME").should("exist");
    });

    it("should not remember the user when 'stay signed-in' option is not selected when login", function () {
      loginPage.loginWithoutStaySignedIn(userName, password);
      cy.intercept("GET", "**/instances").as("instances");
      cy.wait("@instances");
      cy.getCookie("CERVELLO_REMEMBER_ME").should("not.exist");
    });

    it("should display login validation errors", { tags: "@smoke" }, () => {
      cy.get(Selectors.usernameField).type("test@test.test").clear().blur();
      cy.contains("span", "Please insert a valid email").should("be.visible");

      cy.get(Selectors.passwordField)
        .type("InvalidPass")
        .clear()
        .blur()
        .parent()
        .should("have.class", "Mui-error Mui-error");
    });

    it("should error for an invalid user", function () {
      loginPage.login("InvalidUser@invalid.user", "InvalidPass");
      cy.contains("span", "Invalid user data!").should("be.visible");
    });

    it("should error for an invalid password for existing user", function () {
      loginPage.login(userName, "InvalidPass");
      cy.contains("span", "Invalid user data!").should("be.visible");
    });
  });

  context("SignUp Tests", () => {
    it("Should navigate to 'Sign-Up' page and confirm sending email to the user's email & Resend Email", () => {
      loginPage.navigateToSignUpPage();
      signUpPage.signUp(newRandomUserEmail);
      magicLinkPage.verifyCheckInboxAppearance();
      cy.contains(Selectors.resendButton).click();
      cy.contains("Sending magic link to your email").should("exist");
    });

    // it("Should display signup errors", function () {
    //   loginPage.navigateToSignUpPage();
    //   signUpPage.signUp("InvalidEmail");
    //   cy.contains("span", "Please insert a valid email").should("be.visible");
    // });
  });

  context("Logout Tests", () => {
    it("Should logout Successfully and navigate to Auth page", () => {
      cy.loginByAPI();
      cy.visit("/");
      cy.get("#layout-dropdown").click();
      cy.contains("Sign out").click();
      cy.url().should("contain", "auth");
    });
  });
});

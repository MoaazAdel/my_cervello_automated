import { Selectors } from "./auth-selectors";

export class SignUpPage {
  signUp(newRandomUserEmail) {
    cy.get(Selectors.signUpEmailField).type(newRandomUserEmail + "{enter}");
  }
}

export const signUpPage = new SignUpPage();

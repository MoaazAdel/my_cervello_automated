/// <reference types="cypress"/>

import { timeouts } from "../../config/timeouts";
import { Selectors } from "./auth-selectors";

export class LoginPage {
  login(userName, password) {
    cy.get(Selectors.usernameField, { timeout: timeouts.medium }).type(
      userName
    );
    cy.get(Selectors.passwordField)
      .compareSnapshot("Just Pass Field")
      .type(password, { log: false });
    cy.get(Selectors.loginBtn).click();
    return this;
  }

  loginWithoutStaySignedIn(userName, password) {
    cy.get(Selectors.usernameField, { timeout: timeouts.medium }).type(
      userName
    );
    if (typeof password !== "string" || !password) {
      throw new Error(
        "Missing Password value, set the Password using CYPRESS_password=..."
      );
    }
    cy.get(Selectors.passwordField).type(password, { log: false });
    cy.get(Selectors.staySignedInOption).click();
    cy.get(Selectors.loginBtn).click();
    return this;
  }

  navigateToSignUpPage() {
    cy.contains("span", "Create Account").click();
  }
}

export const loginPage = new LoginPage();

/// <reference types="cypress"/>

import { timeouts } from "../../config/timeouts";



export class ChooseInstancePage {

  Selectors = {
    WELCOMEMESSAGE: "h3.MuiTypography-root",
  };

  verifyWelcomeBackMessage() {
    cy.get(this.Selectors.WELCOMEMESSAGE, {
      timeout: timeouts.medium,
    }).should("contain.text", "Welcome to Cervello Suite");
    return this;
  }

  openOwnedInstance() {
    cy.get("body").then(($body) => {
      if ($body.find("p:All instances").length > 0) {
        cy.contains("Your instance")
          .parent()
          .siblings()
          .contains("Open")
          .click();
      }
    });
    cy.contains("Welcome to Cervello Suite").should("exist");
  }
}

export const chooseInstancePage = new ChooseInstancePage();

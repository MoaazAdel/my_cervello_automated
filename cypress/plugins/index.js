/// <reference types="cypress" />
/// <reference types="@shelex/cypress-allure-plugin" />
const path = require("path");
const fs = require("fs-extra");
const tagify = require("cypress-tags");

const allureWriter = require("@shelex/cypress-allure-plugin/writer");
const {
  beforeRunHook,
  afterRunHook,
} = require("cypress-mochawesome-reporter/lib");

module.exports = (on, config) => {
  allureWriter(on, config);
  require("cypress-mochawesome-reporter/plugin")(on);
  require("cypress-grep/src/plugin")(config);
  on("before:run", async (details) => {
    console.log("override before:run");
    await beforeRunHook(details);
  });

  on("after:run", async () => {
    console.log("override after:run");
    await afterRunHook();
  });

  on("task", {
    dbQuery: (query) =>
      require("cypress-postgres")(query.query, query.connection),
  });

  return config;

  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
};

// cypress/plugins/index.js

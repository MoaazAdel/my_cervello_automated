import "cypress-localstorage-commands";

// const token = Cypress.env("token");
// const authorization = `Bearer ${token}`;

// Cypress.Commands.add("loginByAPI", () => {
//   const apiHost = Cypress.env("apiUrl");
//   cy.request({
//     method: "POST",
//     url: `${apiHost}/auth`,
//     body: {
//       email: "cer01@qa.team",
//       password: "Iotblue55$",
//       grantType: "password",
//     },
//   }).then((resp) => {
//     window.localStorage.setItem("access_token", resp.body.access_token);
//   });
// });

Cypress.Commands.add("loginByAPI", () => {
  const apiHost = Cypress.env("apiUrl");
  cy.request({
    method: "POST",
    url: `${apiHost}/auth`,
    body: {
      username: Cypress.env("userName"),
      password: Cypress.env("password"),
      grantType: "password",
    },
  }).then((resp) => {
    window.localStorage.setItem("access_token", resp.body.access_token);
    cy.request({
      method: "GET",
      url: `${apiHost}/instances`,
      headers: {
        authorization: resp.body.access_token,
      },
    }).then((instances) => {
      window.localStorage.setItem(
        "ownedInstnace",
        instances.body.result.instanceOwned.id
      );
      console.log(instances.body.result.instanceOwned.id);
    });
  });
});

// Visual Regression
const compareSnapshotCommand = require("cypress-visual-regression/dist/command");

compareSnapshotCommand();
Cypress.Commands.add("fillMaildev", () => {
  return cy.maildevDeleteAllMessages().then(() => {
    return cy.exec(
      `npm run fillEmail --host ${Cypress.env(
        "MAILDEV_HOST"
      )} --port ${Cypress.env("MAILDEV_SMTP_PORT")}`,
      { failOnNonZeroExit: false }
    );
  });
});

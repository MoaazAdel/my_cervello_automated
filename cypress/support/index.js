import "cypress-mochawesome-reporter/register";
// Import commands.js using ES2015 syntax:
import "./commands";
import "@shelex/cypress-allure-plugin";
require("cypress-maildev");

// load and register the grep feature
// https://github.com/cypress-io/cypress-grep
require("cypress-grep")();

beforeEach(function () {
  window.logCalls = 1;
  window.testFlow = [];
});

/// <reference types = "cypress" />

declare namespace Cypress {
    interface Chainable {



        /**
         * @example
         * Modified Method for requesting API with different Object
         * Pass the object at the header
         * Modify the object using Spread ...object
         * @example
         * cy.apiRequest({
            ...baseObject,
            body: { ...baseObject.body, anotherKey: anotherValue },
            failOnStatusCode: false,
            }).then);
         */
        apiRequest();

        /**
         * Login using APIs to be used before UI or API tests 
         * To reduce the time needed for tests
         * @example
         * cy
         *   .loginByAPI()
         */
        loginByAPI();

    }
}